---toml
title = "Change of webserver from Apache to Nginx"
date = "2019-02-19T17:00:00.000Z"
severity = "under-maintenance"
affectedsystems = ["nextcloud"]
scheduled = "2019-02-19T17:00:00.000Z"
duration = "60"
resolved = true
---
Moved Nextcloud's webserver from Apache to Nginx. Much better performance, also reverse proxy.

<!--- language code: en -->
