---toml
title = "DietPi userdata permissions error"
date = "2019-02-25T16:32:43.174Z"
severity = "major-outage"
affectedsystems = ["nextcloud"]
resolved = true

---

After unsuccessful installation of Docker, everything is on fire.

::: update Resolved | 2019-02-25T16:48:43.174Z
`chgrp`'d some dirs, restarted services, re-did the backup for good measure.
:::

<!--- language code: en -->
